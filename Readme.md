## E-commerce Scrapper
To gather data of products from e-commerce websites.

### Setup the project
* Install below dependency
    * python 3.6
    * run the below command
    ```
      cd flipkart
      pip install -r requirements.txt
    ```
### Run the project

*
    ```
        cd spiders
        scrapy runspider scrapeflipkart.py
    ```

### Snapshot of scrapped mobile data

Currently, it scrape only data of four brands Mi, Vivo, Oppo, Samsung.

![data_1](https://gitlab.com/tejaschauhan/e-commerce-scrapper/-/blob/master/images/smart_phone_data_1.png?raw=true)

![data_2](https://gitlab.com/tejaschauhan/e-commerce-scrapper/-/blob/master/images/smart_phone_data_2.png?raw=true)

![data_3](https://gitlab.com/tejaschauhan/e-commerce-scrapper/-/blob/master/images/smart_phone_data_3.png)
